# This Docker Compose file defines the setup for a multi-container application.
# It includes services for a FastAPI application, a PostgreSQL database, pgAdmin for database management,
# MongoDB as a NoSQL database, and Mongo-Express for MongoDB management.


  #Define persistent storage:
  # - 'twoshot' volume for application data.
  # - 'pg-admin' volume for pgAdmin data.
  # - 'mongo-data' volume for MongoDB data.

  # Define services:
  # - FastAPI application:
  #   - Build the image from the 'fastapi' directory using 'Dockerfile.dev'.
  #   - Expose the application on port 8000.
  #   - Mount the application code for live editing.
  #   - Set environment variables for database connections.

  # - PostgreSQL database:
  #   - Use the PostgreSQL 14.2 image.
  #   - Initialize the database with 'init.sql'.
  #   - Set up user, password, and database name.
  #   - Expose the database on port 15432.

  # - pgAdmin:
  #   - Use the pgAdmin 4 image.
  #   - Store data in the 'pg-admin' volume.
  #   - Access pgAdmin on port 8082.
  #   - Set up default login credentials.

  # - MongoDB:
  #   - Use the latest MongoDB image.
  #   - Store data in the 'mongo-data' volume.
  #   - Set up root user and password.
  #   - Expose MongoDB on port 27017.

  # - Mongo Express:
  #   - Use the latest Mongo Express image.
  #   - Ensure it starts after MongoDB is available.
  #   - Set up admin credentials.
  #   - Connect to the MongoDB service.
  #   - Expose Mongo Express on port 8081.


# Define volumes that will be used by the services.
# These volumes are marked as external, which means they should be created manually before starting the services.
volumes:
  twoshot:
    external: true
  pg-admin:
    external: true
  mongo-data:
    external: true

# Define the services that make up the application.
services:
  # FastAPI service configuration
  fastapi:
    build: # Instructions to build the FastAPI Docker image
      context: fastapi # The build context directory
      dockerfile: Dockerfile.dev # The Dockerfile to use for the build
    ports:
      - "8000:8000" # Map port 8000 of the host to port 8000 of the container
    volumes:
      - ./fastapi:/app # Mount the local 'fastapi' directory to '/app' in the container
    environment: # Environment variables for the FastAPI service
      DATABASE_URL: postgresql://user:password@postgres:5432/twoshot # PostgreSQL connection string
      WAIT_HOSTS: postgres:5432 # Hosts to wait for before starting the service
      MONGO_URL: mongodb://mongouser:mongopassword@mongo/admin # MongoDB connection string

  # PostgreSQL service configuration
  postgres:
    image: postgres:14.2-bullseye # Use the specified PostgreSQL image
    volumes:
      - ./init.sql:/docker-entrypoint-initdb.d/init.sql # Initialize the database with the provided SQL script
    environment: # Environment variables for the PostgreSQL service
      POSTGRES_USER: user # Default user for PostgreSQL
      POSTGRES_PASSWORD: password # Password for the default user
      POSTGRES_DB: twoshot # Default database name
    ports:
      - "15432:5432" # Map port 15432 of the host to port 5432 of the container

  # pgAdmin service configuration
  pg-admin:
    image: dpage/pgadmin4 # Use the specified pgAdmin image
    volumes:
      - pg-admin:/var/lib/pgadmin # Use the 'pg-admin' volume for pgAdmin data
    ports:
      - 8082:80 # Map port 8082 of the host to port 80 of the container
    environment: # Environment variables for the pgAdmin service
      PGADMIN_DEFAULT_EMAIL: me@me.com # Default email for pgAdmin login
      PGADMIN_DEFAULT_PASSWORD: password # Password for the default email
      PGADMIN_DISABLE_POSTFIX:  1 # Disable postfix service within the container

  # MongoDB service configuration
  mongo:
    image: mongo:latest # Use the latest MongoDB image
    volumes:
      - mongo-data:/data/db # Use the 'mongo-data' volume for MongoDB data
    environment: # Environment variables for the MongoDB service
      MONGO_INITDB_ROOT_USERNAME: mongouser # Root username for MongoDB
      MONGO_INITDB_ROOT_PASSWORD: mongopassword # Password for the root user
    ports:
      - "27017:27017" # Map port 27017 of the host to port 27017 of the container

  # Mongo Express service configuration
  mongo-express:
    image: mongo-express:latest # Use the latest Mongo Express image
    depends_on:
      - mongo # Specify that Mongo Express depends on the MongoDB service
    environment: # Environment variables for the Mongo Express service
      ME_CONFIG_MONGODB_ADMINUSERNAME: mongouser # Admin username for Mongo Express
      ME_CONFIG_MONGODB_ADMINPASSWORD: mongopassword # Password for the admin user
      ME_CONFIG_MONGODB_SERVER: mongo # The MongoDB server to connect to
    ports:
      - "8081:8081" # Map port 8081 of the host to port 8081 of the container
    
  